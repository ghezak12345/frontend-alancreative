import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Navbar from './paritals/Navbar';
import ListProducts from './views/Products/ListProducts';
import Transaction from './views/Transaksi/Transaction';
import AddProduct from './views/Products/AddProduct';
import EditProduct from './views/Products/EditProduct';

function App() {
  return (
    <Router>
      
      <Navbar />
      
      <div className="container mt-5">
        <Routes>
          <Route exact path="/" element={<ListProducts />} />
          <Route path="/transaksi" element={<Transaction />} />
          <Route path="/add_product" element={<AddProduct />} />
          <Route path="/product/edit/:id" element={<EditProduct />}/>,
        </Routes>
      </div>
    </Router>
  );
}

export default App;
