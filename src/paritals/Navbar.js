import { NavLink } from "react-router-dom";

const Navbar =  () => {
    return(
        <>
        <nav className="navbar navbar-expand-lg navbar-light bg-info">
            <div className="container">
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <a className="navbar-brand text-light fw-bold" href="/#">Alan Resto</a>
            </div>
        </nav>
        <nav className="navbar navbar-expand-lg navbar-light bg-white border-bottom">
            <div className="container">
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                        <NavLink className="nav-link fw-bold" to="/" activeclassname="active"> Food </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link fw-bold" to="/transaksi" activeclassname="active">Transaksi</NavLink>
                    </li>
                </ul>
                </div>
            </div>
        </nav>
        </>
    );
}

export default Navbar;