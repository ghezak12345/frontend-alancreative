import Dropzone from "react-dropzone-uploader";

import 'react-dropzone-uploader/dist/styles.css';

const MyDropzone = ( { handleFile } ) => {
    const handleFileUpload = (file) => {
        handleFile(file.file);
    };

    return (
        <div>
            <Dropzone
                onChangeStatus={handleFileUpload}
                accept="image/*"
                maxFiles={1}
                multiple={false}
                inputContent="Drag and drop an image or click to select"
                styles={{
                    dropzone: {
                        minHeight: 200,
                        border: '2px dashed #ddd',
                        borderRadius: 4,
                        overflow: 'hidden',
                        padding: '20px',
                        textAlign: 'center',
                    },
                    inputLabel: {
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: '#999',
                    },
            }}
            />
        </div>
    );
};

export default MyDropzone;