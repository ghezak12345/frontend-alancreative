import { faPencil, faPlus, faTrash } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useCallback, useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";

const ListProducts = () => {

    const URLLink = process.env.REACT_APP_API_URL;
    const [product, setProduct] = useState(null);

    const fetchData = useCallback(async () => {
        try {
            const urlProduct = URLLink+"/api/products";
            const optionClasses  = {method:'GET',headers: {'Content-Type': 'application/json','X-Requested-With':'XMLHttpRequest'},credentials: 'include'}
            const res = await fetch(urlProduct, optionClasses);
            if (res.ok) {
                const json = await res.json();
                setProduct(json);
            }else{
                console.log(res);
            }
                    
        } catch (error) {
            console.log(error);
        }
    }, [URLLink])
    
    useEffect(() => {
        fetchData();
    }, [fetchData]);
    
    const handleDelete = async (id) => {
        const res = await fetch(URLLink+'/api/delete_product', {
            method: 'POST',
                body: JSON.stringify({id: id}),
                headers: {'Content-Type': 'application/json','X-Requested-With':'XMLHttpRequest'},
                credentials: 'include',
            });

            if(!res.ok){
                console.log(res);
            }else{
                fetchData();
            }
    }

    return(
        <>
        <p className="text-muted"> Tambahkan Menu makanan yang ada di resto</p>
        <div className="row">
            <div className="col-12">
                <div className="card p-4">
                    <div className="card-body">
                        <NavLink to="/add_product" className="btn btn-info text-light"> <FontAwesomeIcon icon={faPlus} /> Tambah Menu</NavLink>

                        <table className="table table-striped table-bordered mt-4 custom-table">
                            <thead className="custom-thead">
                                <tr>
                                    <td style={{width: '5%'}}>#</td>
                                    <td style={{width: '45%'}}>Nama</td>
                                    <td style={{width: '20%'}}>Foto</td>
                                    <td style={{width: '20%'}}>Harga</td>
                                    <td style={{width: '10%'}}>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                {product && product.data.map((productt, index) => {
                                    const urutan = index + 1;
                                    const hargaFormatted = new Intl.NumberFormat("id-ID", {
                                        style: "currency",
                                        currency: "IDR",
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    }).format(productt.harga);

                                    return(
                                        <tr key={index}>
                                            <td>{urutan}</td>
                                            <td>{productt.nama}</td>
                                            <td><img alt="product_foto" src={URLLink+"/storage/"+productt.foto} className="w-25"/></td>
                                            <td>{hargaFormatted}</td>
                                            <td>
                                                <Link to={`/product/edit/${productt.id}`} className="btn btn-sm btn-primary"> <FontAwesomeIcon icon={faPencil} /> </Link>
                                                <Link to="#" onClick={() => handleDelete(productt.id)} className="btn btn-sm btn-danger"> <FontAwesomeIcon icon={faTrash} /> </Link>
                                            </td>
                                        </tr>
                                    )
                                })}

                                {product && product.data.length === 0 && 
                                <tr key="1">
                                    <td colSpan='4' className="text-center">{product.message}</td>
                                </tr>
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </>
    );
}

export default ListProducts