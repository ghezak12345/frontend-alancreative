import { useEffect, useRef, useState } from "react";
import MyDropzone from "../../utils/MyDropzone ";
import { useNavigate } from "react-router-dom";

const FormProduct = (props) => {

    const form = useRef(null);
    const { effact, id, url } = props;
    const URLLink = process.env.REACT_APP_API_URL;

    const navigate = useNavigate();
    const [harga, setHarga] = useState(0);
    const [nama, setNama] = useState('');
    const [foto, setFoto] = useState('');
    const [selectedFile, setSelectedFile] = useState(null);

    const handleChangeHarga = (event) => {
        const rawValue = event.target.value;
        // Remove non-digit characters from the raw value
        const cleanValue = rawValue.replace(/\D/g, '');
        // Parse the clean value as an integer
        const parsedValue = parseInt(cleanValue, 10);
        // Format the parsed value using toLocaleString
        const formattedValue = parsedValue.toLocaleString('id-ID');
        // Set the formatted value as the input value
        setHarga(formattedValue);
    };

    const handleFile = (file) => {
        setSelectedFile(file);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = new FormData(form.current);
        if (selectedFile) {
            data.append("foto", selectedFile);
        }

        if(effact){
            data.append("id", id);
        }

        data.append("harga", harga.replace(/\D/g, ''));
        const response = await fetch(url, {
            method: 'POST',
            body: data,
            headers: {'X-Requested-With':'XMLHttpRequest'},
            credentials: 'include'
        })
        if (!response.ok) { // error coming back from server
            console.log(response)
        }else{
            navigate('/');
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                const urlProduct = process.env.REACT_APP_API_URL+"/api/detail_product";
                const optionClasses  = {method:'POST',headers: {'Content-Type': 'application/json','X-Requested-With':'XMLHttpRequest'},credentials: 'include', body: JSON.stringify({id: id})};
                const res = await fetch(urlProduct, optionClasses);
                if (res.ok) {
                    const json = await res.json();
                    setNama(json.data.nama);
                    setFoto(json.data.foto);
                    handleChangeHarga({ target: { value: json.data.harga } }); 
                }else{
                    console.log(res);
                }
                        
            } catch (error) {
                console.log(error);
            }
        }

        if(effact){
            fetchData();
        }
    }, [effact, id]);

    return (
        <form ref={form} onSubmit={handleSubmit} autoComplete="off">
            <div className="mb-3">
                <label className="form-label">Nama Menu</label>
                <input type="text" name="nama" className="form-control" value={nama} onChange={(e) => { setNama(e.target.value); }}/>
            </div>
            <div className="mb-3">
                <label className="form-label" style={{display: 'block'}}>Foto Menu</label>
                {foto && <img src={URLLink+"/storage/"+foto} style={{width: '100px'}} className="img-thumbnail" alt="Product" />}
                <MyDropzone handleFile={handleFile} />
            </div>
            <div className="mb-5">
                <label className="form-label">Harga Menu</label>
                <div className="input-group">
                    <div className="input-group-text bg-info">Rp.</div>
                    <input type="text" name="harga" className="form-control" value={harga} onChange={handleChangeHarga} />
                </div>
            </div>
            <div className="col-12">
                <button type="submit" className="btn btn-success float-end">Simpan
            </button>
            </div>
        </form>
    );

}

export default FormProduct;