import { useState } from "react";
import { useParams } from "react-router-dom";
import FormProduct from "./FormProduct";

const EditProduct = () => {

    const { id } = useParams();

    const [URLLink] = useState(process.env.REACT_APP_API_URL+'/api/update_product');
    
    return (
        <div className="row">
            <div className="col-12">
                <div className="card p-4">
                    <div className="card-body">
                        <p className="text-info fw-bold mb-3">Ubah Menu</p>

                        <FormProduct effact={true} id={id} url={URLLink} />

                    </div>
                </div>
            </div>
        </div>
    );

}

export default EditProduct;