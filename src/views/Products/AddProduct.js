import { useState } from "react";
import FormProduct from "./FormProduct";

const AddProduct = () => {
    
    const [URLLink] = useState(process.env.REACT_APP_API_URL+'/api/update_product');

    return (
        <div className="row">
            <div className="col-12">
                <div className="card p-4">
                    <div className="card-body">
                        <p className="text-info fw-bold mb-3">Tambahkan Menu</p>
                        <FormProduct effact={false} id={null} url={URLLink} />
                    </div>
                </div>
            </div>
        </div>
    );

}

export default AddProduct;