import { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";


const Transaction = () => {

    const URLLink = process.env.REACT_APP_API_URL;
    const [product, setProduct] = useState(null);
    const [selectedProducts, setSelectedProducts] = useState(() => {
        const storedProducts = localStorage.getItem("selectedProduct");
        return storedProducts ? JSON.parse(storedProducts) : [];
    });
    const [showModal, setShowModal] = useState(false);
    
    const [uangpembeli, setUangPembeli] = useState(0);
    const [kembalian, setKembalian] = useState(0);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const urlProduct = URLLink+"/api/products";
                const optionClasses  = {method:'GET',headers: {'Content-Type': 'application/json','X-Requested-With':'XMLHttpRequest'},credentials: 'include'}
                const res = await fetch(urlProduct, optionClasses);
                if (res.ok) {
                    const json = await res.json();
                    setProduct(json);
                }else{
                    console.log(res);
                }
                        
            } catch (error) {
                console.log(error);
            }
        }

        fetchData();
    }, [URLLink]);

    const handleProductClick = (product) => {
        setSelectedProducts((prevSelectedProducts) => {
        const existingProduct = prevSelectedProducts.find((p) => p.id === product.id);
    
        if (existingProduct) {
        // If the product already exists in the selectedProducts array, update its quantity and total price
        const updatedProducts = prevSelectedProducts.map((p) => {
            if (p.id === product.id) {
                return {
                    ...p,
                    quantity: p.quantity + 1,
                    totalPrice: (p.quantity + 1) * p.price,
                };
                }
                return p;
            });
        
            return updatedProducts;
            } else {
            // If the product doesn't exist in the selectedProducts array, add it with quantity 1 and calculate the total price
            const newProduct = {
                ...product,
                quantity: 1,
                totalPrice: product.price,
            };
        
            const updatedProducts = [...prevSelectedProducts, newProduct];
            return updatedProducts;
            }
        });
    };

    const clearCart = () => {
        localStorage.removeItem("selectedProduct");
        setSelectedProducts([]);
    };

    const printReceipt = () => {
        // Create a new window for printing
        const printWindow = window.open("", "_blank");
    
        // Generate the receipt content
        const receiptContent = `
            <html>
            <head>
                <title>Receipt</title>
                <style>
                /* Define the styles for the receipt */
                /* You can customize the styles as per your requirements */
                .receipt {
                    font-family: Arial, sans-serif;
                    font-size: 12px;
                }
                .receipt-item {
                    display: flex;
                    justify-content: space-between;
                    margin-bottom: 5px;
                }
                .receipt-item span {
                    flex: 1;
                }
                .receipt-total {
                    font-weight: bold;
                    margin-top: 10px;
                }
                </style>
            </head>
            <body>
                <div class="receipt">
                <h2>Receipt</h2>
                <hr>
                ${selectedProducts.map((product, index) => `
                    <div class="receipt-item">
                    <span>${product.nama} x ${product.quantity}</span>
                    <span>Rp. ${(product.harga * product.quantity).toLocaleString("id-ID")}</span>
                    </div>
                `).join('')}
                <hr>
                <div class="receipt-total">
                    Total: Rp. ${selectedProducts.reduce((total, product) => total + (product.harga * product.quantity), 0).toLocaleString("id-ID")}
                </div>
                </div>
            </body>
            </html>
        `;
    
        // Write the receipt content to the new window
        printWindow.document.open();
        printWindow.document.write(receiptContent);
        printWindow.document.close();
    
        // Print the receipt
        printWindow.print();
    };

    const saveReceipt = () => {
        // Generate the receipt content
        const receiptContent = `
            Receipt\n
            -----------------------------------\n
            ${selectedProducts.map((product, index) => `
                ${product.nama} x ${product.quantity} = Rp. ${(product.harga * product.quantity).toLocaleString("id-ID")}\n
            `).join('')}
            -----------------------------------\n
            Total: Rp. ${selectedProducts.reduce((total, product) => total + (product.harga * product.quantity), 0).toLocaleString("id-ID")}
        `;
    
        // Create a Blob from the receipt content
        const blob = new Blob([receiptContent], { type: 'text/plain' });
    
        // Create a temporary URL for the Blob
        const url = URL.createObjectURL(blob);
    
        // Create a temporary link element
        const link = document.createElement('a');
        link.href = url;
        link.download = 'receipt.txt';
    
        // Programmatically click the link to trigger the download
        link.click();
    
        // Clean up by revoking the URL
        URL.revokeObjectURL(url);
    };

    const handleCharge = () => {
        setShowModal(true);
    };    
    
    const handleCloseModal = () => {
        setShowModal(false);
        setUangPembeli(0);
        setKembalian(0);
    };

    const handleChangeUangPembeli = (event) => {
        const rawValue = event.target.value;
        // Remove non-digit characters from the raw value
        const cleanValue = rawValue.replace(/\D/g, '');
        // Parse the clean value as an integer
        const parsedValue = parseInt(cleanValue, 10);
        // Format the parsed value using toLocaleString
        const formattedValue = parsedValue.toLocaleString('id-ID');
        // Set the formatted value as the input value
        setUangPembeli(formattedValue);
    };
    
    const handlePay = () => {
        const total = selectedProducts.reduce((acc, product) => acc + product.harga * product.quantity, 0);
        const numericValue = Number(uangpembeli.replace(/\D/g, ''));
        const kembalian = numericValue - total; // Calculate the change
        
        // Format the parsed value using toLocaleString
        const formattedValue = kembalian.toLocaleString('id-ID');

        setKembalian(formattedValue);
    };

    useEffect(() => {
        localStorage.setItem("selectedProduct", JSON.stringify(selectedProducts));
    }, [selectedProducts]);

    return(
        <>
        <div className="row">
            <div className="col-8">
                <div className="row">
                {product && product.data.map((productt, index) => {
                    const hargaFormatted = new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    }).format(productt.harga)
                    return(
                        <div className="col-4" key={index}>
                            <div className="card" onClick={() => handleProductClick(productt)}>
                                <img
                                    src={URLLink+"/storage/"+productt.foto}
                                    className="card-img-top"
                                    alt="Selected file"
                                    style={{ height: "200px", objectFit: "cover" }}
                                />
                                <div className="card-body text-center">
                                    <h5 className="card-title">{productt.nama}</h5>
                                    <p className="card-text fw-bold text-info">{hargaFormatted}</p>
                                </div>
                            </div>
                        </div>
                    );
                })}
                </div>
            </div>
            <div className="col-4">
                <div className="card p-4">
                    <div className="card-body">
                        <h4 className="text-center mb-5">Pesanan</h4>
                        {selectedProducts && selectedProducts.length > 0 ? (
                            <table className="col-12 mb-5">
                                <tbody>
                                {selectedProducts.map((product, index) => {
                                    const hargaFormatted = new Intl.NumberFormat("id-ID", {
                                        style: "currency",
                                        currency: "IDR",
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    }).format(product.harga * product.quantity); // Calculate the total price
                                    
                                    return (
                                        <tr key={index}>
                                        <td style={{ width: "30%" }}>
                                            <img
                                            alt="product_foto"
                                            src={URLLink + "/storage/" + product.foto}
                                            className="w-100"
                                            />
                                        </td>
                                        <td style={{ width: "38%" }} className="fw-bold">
                                            {product.nama}
                                        </td>
                                        <td style={{ width: "7%" }} className="fw-bold">
                                            x{product.quantity} {/* Display the quantity */}
                                        </td>
                                        <td style={{ width: "30%" }} className="fw-bold text-info">
                                            Rp. {hargaFormatted} {/* Display the total price */}
                                        </td>
                                        </tr>
                                    );
                                })}
                                </tbody>
                            </table>
                            ) : (
                            <p>No selected products</p>
                        )}
                        <button type="button" className="btn btn-outline-danger col-12" onClick={clearCart}><span className="fw-bold">Clear Cart</span></button>
                        <table className="col-12 mt-3">
                            <tbody>
                                <tr>
                                    <td><button type="button" className="btn btn-success col-12" onClick={printReceipt} disabled={selectedProducts.length === 0}><span className="fw-bold">Print Bill</span></button></td>
                                    <td><button type="button" className="btn btn-success col-12" onClick={saveReceipt} disabled={selectedProducts.length === 0}><span className="fw-bold">Save Bill</span></button></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" className="btn btn-info col-12 mt-3" onClick={handleCharge}>
                            <span className="text-white fw-bold">Charge</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <Modal show={showModal} onHide={handleCloseModal} size="lg">
        <Modal.Header closeButton>
            <Modal.Title>Detail Pesanan</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            {/* Place your content for the charge modal here */}
            <div className="row">
                <div className="col-8">
                <table className="table table-striped table-bordered mt-4 custom-table">
                    <thead className="custom-thead">
                        <tr>
                        <td style={{ width: '5%' }}>#</td>
                        <td style={{ width: '50%' }}>Nama</td>
                        <td style={{ width: '25%' }}>Foto</td>
                        <td style={{ width: '20%' }}>Harga</td>
                        </tr>
                    </thead>
                    <tbody>
                        {selectedProducts.map((product, index) => {
                        const totalPrice = product.harga * product.quantity;
                        const formattedPrice = new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR",
                        }).format(totalPrice);

                        return (
                            <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{product.nama} x{product.quantity}</td>
                            <td>
                                <img
                                alt="product_foto"
                                src={URLLink + "/storage/" + product.foto}
                                className="w-100"
                                />
                            </td>
                            <td>Rp. {formattedPrice}</td>
                            </tr>
                        );
                        })}
                    </tbody>
                </table>
                </div>
                <div className="col-4">
                    <h5 className="text-center mt-4">Uang Pembeli (Rp)</h5>
                    <input type="text" className="form-control" value={uangpembeli} onChange={handleChangeUangPembeli}/>
                    <table className="col-12 mt-3">
                        <tbody>
                            <tr>
                                <td><button type="button" className="btn btn-outline-secondary col-12" onClick={handleCloseModal}><span className="fw-bold">Close</span></button></td>
                                <td><button type="button" className="btn btn-info col-12" onClick={handlePay}><span className="fw-bold text-white">Pay ! </span></button></td>
                            </tr>
                        </tbody>
                    </table>
                    <p className="mt-3 text-danger fw-bold">Kembalian : Rp. {kembalian}</p>
                </div>
            </div>
        </Modal.Body>
        </Modal>
        </>
    )

}

export default Transaction;